motor==2.0.0
pika==0.13.1
pkg-resources==0.0.0
pyaes==1.6.1
pyasn1==0.4.5
pymongo==3.7.2
rsa==4.0
Telethon==1.6.2
