import os
import asyncio
from telethon import TelegramClient, sync, utils
# from telethon.tl.functions.contacts import ResolveUsernameRequest
# import telethon
# import telethon.sync
import motor.motor_asyncio


class MongoAsyncClientForTelegram():
    SETTINGS = dict(
        mongoIP="127.0.0.1",
        mongoPort=27017,
    )

    def __init__(self, mongoIP=SETTINGS["mongoIP"], mongoPort=SETTINGS["mongoPort"]):
        mongo = motor.motor_asyncio.AsyncIOMotorClient('localhost', 27017)
        self.telegram_mess_db = mongo.telegram_mess_db


class InitAsyncTelegramClient():
    SETTINGS = dict(
        api_id="",
        api_hash='',
        phone='+7',
        session_name='template',
    )

    def __init__(self, session_name, api_id=SETTINGS['api_id'], api_hash=SETTINGS['api_hash'], phone=SETTINGS['phone']):
        self.session_name = session_name
        self.file_name = f"{self.session_name}.session"
        self.api_hash = api_hash
        self.api_id = api_id
        self.phone = phone
        self.client = TelegramClient(
            self.session_name, self.api_id, self.api_hash)

    def __make_session_db(self):
        if not os.path.isfile(self.file_name):
            command = f"cp ./template.session ./{self.file_name}"
            os.system(command)

    def __del_session_db(self):
        if os.path.isfile(self.file_name):
            command = f"rm {self.file_name}"
            os.system(command)

    async def start(self):
        self.__make_session_db()
        await self.client.start(phone=self.phone)
        return self.client

    async def stop(self):
        self.__del_session_db()
        await self.client.disconnect()


class AsyncTelegramMessageCollector(MongoAsyncClientForTelegram):
    def __init__(self, chat_id):
        super().__init__()
        self.chat_id = chat_id.replace('@', '')
        self.chat = self.telegram_mess_db[self.chat_id]
        
    async def __save_messages(self, iter_messages):
        
        async for message in iter_messages:
            is_mess_in_db = await self.chat.find_one({"mess_id": message.id})
            is_first_mess_in_db = await self.chat.find_one({"mess_id": 1})
            if is_mess_in_db and is_first_mess_in_db:
                break
            elif is_mess_in_db and not is_first_mess_in_db:
                print(f"in db:{message.id}")
                continue
            mess = message.message
            if isinstance(mess, str) and mess:
                message_dict = {
                    "type": "message",
                    "name": utils.get_display_name(message.sender),
                    "name_id": message.from_id,
                    "mess_id": message.id,
                    "mess_date": message.date,
                    "message":  mess,
                    "reply_to_msg_id": message.reply_to_msg_id,
                    "check_contact": False,
                    "check_black_word": False,
                }
                await self.chat.insert_one(message_dict)
                print(self.chat_id, message_dict)
                
    async def start(self):
        telegram_client = InitAsyncTelegramClient(self.chat_id)
        client = await telegram_client.start()
        entity = await client.get_entity(self.chat_id)
        iter_messages = client.iter_messages(entity)
        await self.__save_messages(iter_messages)
        await telegram_client.stop()


class MakeAsyncLoops():
    def __init__(self, values):
        self.values = values

    def __create_tasks_list(self):
        tasks = []
        for value in self.values:
            obj = AsyncTelegramMessageCollector(value)
            tasks.append(obj.start())
        return tasks

    async def __create_treads(self):
        tasks = self.__create_tasks_list()
        await asyncio.gather(*tasks)

    def start(self):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.__create_treads())


def main():
    # "@KharkovChat", "@spbbiz", '@ponyrznspb',"@msk24", "@piterchat", "@chat_msk", "@nedimon_msk", "@politach", "")
    values = ("@chat_msk", "@nedimon_msk")
    
    loops = MakeAsyncLoops(values)
    loops.start()


if __name__ == '__main__':
    main()
