import time
import json
import pika
from async_telegram_message_collector import AsyncTelegramMessageCollector

connection = pika.BlockingConnection(pika.ConnectionParameters(
    host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='telegram_message_collector', durable=True)

print(' [*] Waiting for messages. To exit press CTRL+C')


def callback(ch, method, properties, body):
    body = json.loads(body)
    values = body.get("value", [])
    print(f"### values: {values}")
    worker = AsyncTelegramMessageCollector(values)
    worker.start()
    ch.basic_ack(delivery_tag=method.delivery_tag)


channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback,
                      queue='telegram_message_collector')

channel.start_consuming()
